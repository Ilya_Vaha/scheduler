package com.epam.scheduler.logic.impl;

import com.epam.scheduler.dao.SchedulerDao;
import com.epam.scheduler.logic.SchedulerService;
import com.epam.scheduler.model.User;
import org.hibernate.exception.DataException;
import org.springframework.transaction.annotation.Transactional;

public class MentorServiceImpl implements SchedulerService {

    private SchedulerDao schedulerDao;

    public void setSchedulerDao(SchedulerDao schedulerDao) {
        this.schedulerDao = schedulerDao;
    }

    @Transactional
    @Override
    public User findUser(int id) throws DataException {
        return schedulerDao.findUser(id);
    }

    @Override
    public void addUser(User user) throws DataException {
        if(user != null){
            schedulerDao.addUser(user);
        }
    }
}
