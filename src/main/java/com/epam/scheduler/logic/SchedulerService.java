package com.epam.scheduler.logic;

import com.epam.scheduler.model.User;
import org.hibernate.exception.DataException;

public interface SchedulerService {

    public User findUser(int id) throws DataException;
    public void addUser(User user) throws DataException;
}
