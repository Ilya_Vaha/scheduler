package com.epam.scheduler.dao.impl;

import com.epam.scheduler.dao.SchedulerDao;
import com.epam.scheduler.model.User;
import org.hibernate.SessionFactory;
import org.hibernate.exception.DataException;

import java.util.Set;

public class MentorDaoImpl implements SchedulerDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User findUser(int id) throws DataException {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Override
    public Set<User> findListUsers(int position, int count) throws DataException {
        return null;
    }

    @Override
    public void addUser(User user) throws DataException {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public int findCountUsers() throws DataException {
        return 0;
    }

}
