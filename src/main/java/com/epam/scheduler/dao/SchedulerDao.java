package com.epam.scheduler.dao;

import com.epam.scheduler.model.User;
import org.hibernate.exception.DataException;

import java.util.Set;

public interface SchedulerDao {

    public User findUser(int id) throws DataException;
    public Set<User> findListUsers(int position, int count) throws DataException;
    public void addUser(User user) throws DataException;
    public int findCountUsers() throws DataException;

}
