package com.epam.scheduler.model;

public enum RoleType {
    ADMIN, MENTOR, TRAINER
}
