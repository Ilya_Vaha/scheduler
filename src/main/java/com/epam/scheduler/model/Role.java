package com.epam.scheduler.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="role")
public class Role implements Serializable {

    @Id
    @Column(name = "role_id", unique = true, nullable = false)
    private int roleId;

    @Enumerated(EnumType.STRING)
    @Column(name="role_name", unique = true, nullable = false)
    private RoleType roleType;

    @ManyToMany(mappedBy="roles")
    private Set<User> users = new HashSet<>();
   /* @ManyToOne
    @JoinColumn(name="user_id")
    private User user;*/

    public int getRoleId() {
        return roleId;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

}
