package com.epam.scheduler.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "address")
public class Address implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "address_id", unique = true, nullable = false)
    private int addressId;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Column(name = "address_name", nullable = false, length = 20)
    private String street;

    @Column(name = "address_corp", nullable = false, length = 5)
    private String corp;

    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy = "address")
    private Set<User> users = new HashSet<>();


    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int id) {
        this.addressId = addressId;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCorp() {
        return corp;
    }

    public void setCorp(String corp) {
        this.corp = corp;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

}
