package com.epam.scheduler.controller;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.SchedulerService;
import com.epam.scheduler.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SchedulerController {


    private static final String SPRING_BEANS = "classpath:spring/cfg/beansLocation.xml";

    @RequestMapping(value = {"/", "/find"}, method = RequestMethod.GET)
    public ModelAndView findOne() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SPRING_BEANS);
        SchedulerService schedulerService = (SchedulerService) context.getBean("mentorServiceImpl");
        long stTime = System.currentTimeMillis();
        User user = schedulerService.findUser(1);
        long fTime = System.currentTimeMillis();
        ModelAndView model = new ModelAndView();
        model.addObject("user", user);
        model.addObject("opTime", fTime - stTime);
        model.setViewName("main");
        return model;
    }

    @ExceptionHandler(DataException.class)
    public ModelAndView handleDataException(DataException ex) {
        ModelAndView model = new ModelAndView();
        model.setViewName("error");
        model.addObject("exception", ex);
        return model;
    }
}
