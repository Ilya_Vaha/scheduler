package com.epam.scheduler.util;

import java.util.ResourceBundle;

public class ConfigurationManager {

	private static final String RESOURCE_BUNDLE = "properties.scheduler";

	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE);

	/**
	 * @param key - a key of the property
	 * @return a property
	 */
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}

}
