<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Index</title>
</head>
<body>
    <h3>Name : ${user.firstName}</h3>
    <h3>Last Name : ${user.lastName}</h3>
    <h3>email : ${user.email}</h3>
    <h3>street : ${user.address.street}</h3>
    <h3>corp : ${user.address.corp}</h3>
    <h3>city : ${user.address.city.name}</h3>
    <h3>opTime : ${opTime}</h3>
</body>
</html>